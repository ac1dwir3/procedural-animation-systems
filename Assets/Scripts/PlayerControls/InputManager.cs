using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Ac1dwir3.PlayerControls
{
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(PlayerController))]
    public class InputManager : MonoBehaviour
    {
        [SerializeField] PlayerController player;

        #region Input Flags
        Vector2 movementInput;
        bool jumped;
        bool sprinting;
        #endregion

        Vector3 movementDirection;

        void Start()
        {
            if (player == null)
                player = GetComponent<PlayerController>();
        }

        void Update()
        {
            Vector3 inputDirection = new Vector3(movementInput.x, 0, movementInput.y);

            if (sprinting) //If sprint button is being pressed
                player.isSprinting = true;
            else //If sprint button is not being pressed
                player.isSprinting = false;

            if (jumped) //If jump button is being pressed
                player.Jump();

            if (inputDirection.magnitude >= 0.1f) //If movement input threshold past
            {
                float targetAngle = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y; //Gets angle to rotate to when moving based on camera angle
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref player.turnSmoothVelocity, player.turnSmoothTime); //Smooths the rotation over time

                if (player.freezeRotation)
                    angle = 0;

                transform.rotation = Quaternion.Euler(0, angle, 0); //Rotates player to face direction of movement

                movementDirection = Quaternion.Euler(0, targetAngle, 0) * Vector3.forward; //Calculates movement direction

                //Allows for variable movement speed
                if (inputDirection.magnitude >= 1)
                    player.Move(movementDirection.normalized);
                else
                    player.Move(movementDirection);
            }
        }

        private void OnDrawGizmos()
        {
            #region Movement Vectors
            //Visualize movement vectors
            Gizmos.color = Color.red;
            if (player.isSprinting)
                Gizmos.DrawRay(transform.position, movementDirection.normalized * 2f); //Longer vectors while sprinting
            else
                Gizmos.DrawRay(transform.position, movementDirection.normalized);

            Gizmos.color = Color.blue;
            Gizmos.DrawRay(transform.position, player.controller.velocity.normalized);
            #endregion
        }

        #region Input Reader Methods
        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }
        public void OnJump(InputAction.CallbackContext context)
        {
            jumped = context.action.triggered;
        }
        public void OnSprint(InputAction.CallbackContext context)
        {
            sprinting = context.action.triggered;
        }
        #endregion
    }
}