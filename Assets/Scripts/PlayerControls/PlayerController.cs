using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Ac1dwir3.PlayerControls
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        [TitleGroup("Locomotion", null, TitleAlignments.Centered, true, true)]
        public CharacterController controller;

        #region Locomotion Variables
        [FoldoutGroup("Movement")][SuffixLabel("m/s", true)] public float movementSpeed;
        [FoldoutGroup("Movement")][SerializeField, SuffixLabel("m/s", true)] float sprintSpeed;
        [FoldoutGroup("Movement")][SerializeField, SuffixLabel("m/s", true)] float inAirSpeed;
        [HideInInspector] public float currentMovementSpeed;

        [FoldoutGroup("Rotation")][SuffixLabel("s", true)] public float turnSmoothTime = 0.1f;
        [FoldoutGroup("Rotation")][HideInInspector] public float turnSmoothVelocity;

        [FoldoutGroup("Grounding")][SerializeField] Transform groundCheck;
        [FoldoutGroup("Grounding")][SerializeField, SuffixLabel("m", true)] float groundDistance = 0.4f;
        [FoldoutGroup("Grounding")][SerializeField] LayerMask groundMask;

        [FoldoutGroup("Jumping")][SerializeField, SuffixLabel("m", true)] float jumpHeight;

        [FoldoutGroup("Debugging")][ReadOnly, SerializeField] Vector3 velocity;
        [FoldoutGroup("Debugging/Movement")][ReadOnly] public bool isSprinting = false;
        [FoldoutGroup("Debugging/Grounding")][ReadOnly] bool isGrounded;
        [FoldoutGroup("Debugging/Falling")][ReadOnly, SerializeField, SuffixLabel("m/s", true)] float gravity = -9.81f;


        [FoldoutGroup("Testing")][SerializeField] bool toggleTesting = false;
        [FoldoutGroup("Testing"), BoxGroup("Testing/Toggles")][SerializeField, ShowIf("toggleTesting")] bool freezeVerticalMovement = false;
        [FoldoutGroup("Testing"), BoxGroup("Testing/Toggles")][SerializeField, ShowIf("toggleTesting")] bool freezeHorizontalMovement = false;
        [FoldoutGroup("Testing"), BoxGroup("Testing/Toggles")][ShowIf("toggleTesting")] public bool freezeRotation = false;
        [FoldoutGroup("Testing"), BoxGroup("Testing/Toggles")][SerializeField, ShowIf("toggleTesting")] bool noJumping = false;
        #endregion

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked; //Hides Mouse Cursor
            controller = GetComponent<CharacterController>();
        }

        void Update()
        {
            #region Grounding
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGrounded && velocity.y < 0)
                velocity.y = -2f;

            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);
            #endregion
        }

        #region Locomotion Methods
        public void Move(Vector3 direction)
        {
            if (freezeHorizontalMovement)
                direction.x = 0;
            if (freezeVerticalMovement)
                direction.z = 0;

            if (isGrounded && !isSprinting) //If grounded and NOT sprinting
                currentMovementSpeed = movementSpeed;//Move with normal speed
            else if (isGrounded && isSprinting) //If grounded and sprinting
                currentMovementSpeed = sprintSpeed;//Move with sprint speed modifier
            else if (!isGrounded) //If in the air
                currentMovementSpeed = inAirSpeed;//Move with air speed ("resistance") modifier

            controller.Move(direction * currentMovementSpeed * Time.deltaTime);
        }
        public void Jump()
        {
            if (noJumping)
                return;

            if (controller.isGrounded) //No jumping while in mid-air
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        private void OnDrawGizmos()
        {
            if (isGrounded)
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;

            Gizmos.DrawWireSphere(groundCheck.position, groundDistance);
        }
        #endregion
    }
}